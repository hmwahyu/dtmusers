<?php


Route::get('/', function () {
    return redirect('users');
});

Auth::routes();

Route::get('/home', function () {
    return redirect('users');
});

Route::get('{path}',"HomeController@index")->where( 'path', '([A-z\d-\/_.]+)?' );
