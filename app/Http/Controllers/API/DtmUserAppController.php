<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DtmUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;

use Validator;


class DtmUserAppController extends Controller
{
  private $confirmSuccess = 'success';
	private $confirmFailed = 'failed';
	private $codeSuccess = 200;
	private $codeFailed = 400;
	private $statusMessage;

  public function __construct()
  {

  }

  public function auth (Request $request){
    $validator = Validator::make($request->all(), [
      'token' => 'required',
      'name' => 'required',
      'device' => 'required',
      'device_id' => 'required',
    ]);
    if ($validator->fails()) {
      $this->setStatusMessage($validator->errors()->first());
      return Response::json([
        'status' => [
          'code' => $this->getCodeFailed(),
          'confirm' => $this->getConfirmFailed(),
          'message' => $this->getStatusMessage(),
        ],
      ], 400);
    }
    $response = $this->cekToken($request,0);

    return $response;
  }

  public function updateUser(Request $request){
    $validator = Validator::make($request->all(), [
      'token' => 'required',
      'name' => 'required',
      'device' => 'required',
      'device_id' => 'required'
    ]);
    if ($validator->fails()) {
      $this->setStatusMessage($validator->errors()->first());
      return Response::json([
        'status' => [
          'code' => $this->getCodeFailed(),
          'confirm' => $this->getConfirmFailed(),
          'message' => $this->getStatusMessage(),
        ],
      ], 400);
    }
    $response = $this->cekToken($request,1);

    return $response;
  }

  private function cekToken($request,$mode){
    try {
    	$client = new Client();
			$res = $client->request('GET', 'https://graph.accountkit.com/v1.3/me/?access_token=' . $request->token,[
				'headers'=>[
					'Content-Type' => 'application/json'
				],
				'timeout'=> 120
			]);
		} catch (ClientException $e) {
      $this->setStatusMessage('Token Expired / Salah');
			return Response::json([
				'status' => [
					'code' => $this->getCodeFailed(),
					'confirm' => $this->getConfirmFailed(),
					'message' => $this->getStatusMessage(),
				],
          'details' => json_decode($e->getResponse()->getBody()->getContents(), true)
			]);
		}
		if($res->getStatusCode() == 200) {
      $phone = $this->getPhone(json_decode($res->getBody()->getContents(), true)['phone']);

      $user = DtmUser::where('phone',$phone) -> first();

      if($user){
        if($user->device_id){
          if($user->device_id != $request->device_id){
            $this->setStatusMessage('Device yang anda gunakan berbeda.');
        		return [
        			'status' => [
        				'code' => $this->getCodeFailed(),
      					'confirm' => $this->getConfirmFailed(),
      					'message' => $this->getStatusMessage(),
        			],
        			'details' => [
                'name' => $user->name,
                'phone' => $user->phone
              ],
            ];
          }
        } else {
          $user->device_id = $request->device_id;
        }
        $today = date("Y-m-d");
        $user->last_login = $today;
        $user->update();
        if($user->isActive == '0'){
          $this->setStatusMessage('Akun Anda Belum Aktif.');
      		return [
      			'status' => [
      				'code' => $this->getCodeFailed(),
    					'confirm' => $this->getConfirmFailed(),
    					'message' => $this->getStatusMessage(),
      			],
      			'details' => [
              'name' => $user->name,
              'phone' => $user->phone
            ],
          ];
        }
        if($user->expired_at <= $today){
          $this->setStatusMessage('Akun Anda Expired.');
      		return [
      			'status' => [
      				'code' => $this->getCodeFailed(),
    					'confirm' => $this->getConfirmFailed(),
    					'message' => $this->getStatusMessage(),
      			],
      			'details' => [
              'name' => $user->name,
              'phone' => $user->phone
            ],
          ];
        }
        if($mode == '1'){
          $user->name = $request->name;
          if($request->address){
            $user->address = $request->address;
          }
          $user->update();
          $user = DtmUser::where('phone',$phone) -> first();
          $user->expired_in = date_diff(date_create(date('Y-m-d')),date_create($user->expired_at))->d . ' Hari';
          $this->setStatusMessage('Success updated user.');
      		return [
      			'status' => [
      				'code' => $this->getCodeSuccess(),
      				'confirm' => $this->getConfirmSuccess(),
      				'message' => $this->getStatusMessage(),
      			],
      			'details' => $user
          ];
        }
      } else if($mode == '0'){
	$today = date("Y-m-d");
        $user = DtmUser::create([
            'name' => $request->name,
            'phone' => $phone,
            'device_id' => $request->device_id,
	    'expired_at' => $today,
        ]);
        $this->setStatusMessage('Akun Anda Belum Aktif.');
    		return [
    			'status' => [
    				'code' => $this->getCodeFailed(),
  					'confirm' => $this->getConfirmFailed(),
  					'message' => $this->getStatusMessage(),
    			],
    			'details' => [
            'name' => $user->name,
            'phone' => $user->phone
          ],
        ];
      } else if($mode == '1'){
        $this->setStatusMessage('Anda belum terdaftar.');
    		return [
    			'status' => [
    				'code' => $this->getCodeFailed(),
  					'confirm' => $this->getConfirmFailed(),
  					'message' => $this->getStatusMessage(),
    			],
    			'details' => [
            'name' => $request->name,
            'phone' => $phone
          ],
        ];
      }
      $user->expired_in = date_diff(date_create(date('Y-m-d')),date_create($user->expired_at))->d . ' Hari';
      $this->setStatusMessage('Success get user.');
  		return [
  			'status' => [
  				'code' => $this->getCodeSuccess(),
  				'confirm' => $this->getConfirmSuccess(),
  				'message' => $this->getStatusMessage(),
  			],
  			'details' => $user
      ];
		}
  }

  private function getPhone($data){
    return '0'.$data['national_number'];
  }

  private function getCodeSuccess() {
	   return $this->codeSuccess;
	}
	private function getCodeFailed() {
		return $this->codeFailed;
	}
	private function getConfirmSuccess() {
		return $this->confirmSuccess;
	}
	private function getConfirmFailed() {
		return $this->confirmFailed;
	}
	private function setStatusMessage($message) {
		(is_array($message) ? $this->statusMessage = $message : $this->statusMessage = array($message));
	}
	private function getStatusMessage() {
		return $this->statusMessage;
	}
}
