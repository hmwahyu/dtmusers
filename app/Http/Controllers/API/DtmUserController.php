<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DtmUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;

use Validator;


class DtmUserController extends Controller
{
  private $confirmSuccess = 'success';
	private $confirmFailed = 'failed';
	private $codeSuccess = 200;
	private $codeFailed = 400;
	private $statusMessage;

  public function __construct()
  {
    $this->middleware('auth:api');
  }

  public function index()
  {
    // $this->authorize('isAdmin');
    if (\Gate::allows('isAdmin')) {
      return DtmUser::latest()->paginate(5);
    }
  }

  public function store(Request $request)
  {
    if (\Gate::allows('isAdmin')) {
      $this->validate($request,[
        'name' => 'required|string|max:191',
        'phone' => 'required|regex:/[0-9]/|unique:dtm_users',
      ]);
	
      $today = date("Y-m-d");
      return DtmUser::create([
        'name' => $request['name'],
        'phone' => $request['phone'],
	'expired_at' => $today,
      ]);
    }
  }

  public function show($id)
  {
    //
  }

  public function update(Request $request, $id)
  {
    if (\Gate::allows('isAdmin')) {
      $user = DtmUser::findOrFail($id);
      $this->validate($request,[
          'name' => 'required|string|max:191',
          'phone' => 'required|regex:/[0-9]/|unique:dtm_users,phone,' . $user->id,
          'expired_at' => 'sometimes|date',
      ]);
      $new = $this->formatDate($request->expired_at);
      $request->merge(['expired_at' => $new]);
      $user->update($request->all());
      return ['message' => 'Updated the user info'];
    }
  }

  public function reset($id)
  {
    // dd('koko');
    if (\Gate::allows('isAdmin')) {
      $user = DtmUser::findOrFail($id);
      if($user){
        $user->device_id = null;
        $user->update();
        return ['message' => ' success reset device'];
      }
      return ['message' => 'gagal reset device'];
    }
  }

  public function destroy($id)
  {
    if (\Gate::allows('isAdmin')) {
      $this->authorize('isAdmin');
      $user = DtmUser::findOrFail($id);
      $user->delete();
      return ['message' => 'DTM User Deleted'];
    }
  }

  public function auth (Request $request){
    $validator = Validator::make($request->all(), [
      'token' => 'required',
      'name' => 'required',
      'device' => 'required',
      'device_id' => 'required',
    ]);
    if ($validator->fails()) {
      $this->setStatusMessage($validator->errors()->first());
      return Response::json([
        'status' => [
          'code' => $this->getCodeFailed(),
          'confirm' => $this->getConfirmFailed(),
          'message' => $this->getStatusMessage(),
        ],
      ], 400);
    }
    $response = $this->cekToken($request->all(),0);

    return $response;
  }

  public function updateUser(Request $request){
    $validator = Validator::make($request->all(), [
      'token' => 'required',
      'name' => 'required',
      'device' => 'required',
      'device_id' => 'required'
    ]);
    if ($validator->fails()) {
      $this->setStatusMessage($validator->errors()->first());
      return Response::json([
        'status' => [
          'code' => $this->getCodeFailed(),
          'confirm' => $this->getConfirmFailed(),
          'message' => $this->getStatusMessage(),
        ],
      ], 400);
    }
    $response = $this->cekToken($request,1);

    return $response;
  }

  private function formatDate($date){
    return date_format(date_create($date),"Y-m-d");
  }

  private function cekToken($request,$mode){
    try {
    	$client = new Client();
			$res = $client->request('GET', 'https://graph.accountkit.com/v1.3/me/?access_token=' . $request->token,[
				'headers'=>[
					'Content-Type' => 'application/json'
				],
				'timeout'=> 120
			]);
		} catch (ClientException $e) {
      $this->setStatusMessage('Token Expired / Salah');
			return Response::json([
				'status' => [
					'code' => $this->getCodeFailed(),
					'confirm' => $this->getConfirmFailed(),
					'message' => $this->getStatusMessage(),
				],
          'details' => json_decode($e->getResponse()->getBody()->getContents(), true)
			]);
		}
		if($res->getStatusCode() == 200) {
      $phone = $this->getPhone(json_decode($res->getBody()->getContents(), true)['phone']);

      $user = DtmUser::where('phone',$phone) -> first();

      if($user){
        $today = date("Y-m-d");
        $user->last_login = $today;
        $user->update();
        if($user->isActive == '0'){
          $this->setStatusMessage('Akun Anda Belum Aktif.');
      		return [
      			'status' => [
      				'code' => $this->getCodeFailed(),
    					'confirm' => $this->getConfirmFailed(),
    					'message' => $this->getStatusMessage(),
      			],
      			'details' => [
              'name' => $user->name,
              'phone' => $user->phone
            ],
          ];
        }
        if($user->expired_at < $today){
          $this->setStatusMessage('Akun Anda Expired.');
      		return [
      			'status' => [
      				'code' => $this->getCodeFailed(),
    					'confirm' => $this->getConfirmFailed(),
    					'message' => $this->getStatusMessage(),
      			],
      			'details' => [
              'name' => $user->name,
              'phone' => $user->phone
            ],
          ];
        }
        if($mode == '1'){
          $user->update($request->all());
          $user = DtmUser::where('phone',$phone) -> first();
          $this->setStatusMessage('Success updated user.');
      		return [
      			'status' => [
      				'code' => $this->getCodeSuccess(),
      				'confirm' => $this->getConfirmSuccess(),
      				'message' => $this->getStatusMessage(),
      			],
      			'details' => $user
          ];
        }
      } else if($mode == '0'){
        $user = DtmUser::create([
            'name' => $request->name,
            'phone' => $phone,
        ]);
        $this->setStatusMessage('Akun Anda Belum Aktif.');
    		return [
    			'status' => [
    				'code' => $this->getCodeFailed(),
  					'confirm' => $this->getConfirmFailed(),
  					'message' => $this->getStatusMessage(),
    			],
    			'details' => [
            'name' => $user->name,
            'phone' => $user->phone
          ],
        ];
      } else if($mode == '1'){
        $this->setStatusMessage('Anda belum terdaftar.');
    		return [
    			'status' => [
    				'code' => $this->getCodeFailed(),
  					'confirm' => $this->getConfirmFailed(),
  					'message' => $this->getStatusMessage(),
    			],
    			'details' => [
            'name' => $user->name,
            'phone' => $user->phone
          ],
        ];
      }
      $this->setStatusMessage('Success get user.');
  		return [
  			'status' => [
  				'code' => $this->getCodeSuccess(),
  				'confirm' => $this->getConfirmSuccess(),
  				'message' => $this->getStatusMessage(),
  			],
  			'details' => $user
      ];
		}
  }

  private function getPhone($data){
    return '0'.$data['national_number'];
  }

  private function getCodeSuccess() {
	   return $this->codeSuccess;
	}
	private function getCodeFailed() {
		return $this->codeFailed;
	}
	private function getConfirmSuccess() {
		return $this->confirmSuccess;
	}
	private function getConfirmFailed() {
		return $this->confirmFailed;
	}
	private function setStatusMessage($message) {
		(is_array($message) ? $this->statusMessage = $message : $this->statusMessage = array($message));
	}
	private function getStatusMessage() {
		return $this->statusMessage;
	}
}
